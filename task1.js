// importing required packages
const jsonfile = require('jsonfile');
const randomstring = require('randomstring')

// variables for inputs and outputs
const inputFile = './input2.json'
const outputFile = './output2.json'

// convert an array to a JSON file.
const createJson = (obj) => {
  jsonfile.writeFile(outputFile, obj)
  .then(res => {
    console.log(`Write complete at: ${outputFile}`)
  })
  .catch(err => console.error(err))
}

// Add random characters to the end of a string. Take the amount of characters you want to add
const addRandomChars = (input, amount) => {
  return input + randomstring.generate(amount)
}

// runtime program to convert and use JSON
jsonfile.readFile(inputFile).then(obj => createRandomEmails(obj))

// create emails object which holds an array of emails. Those emails are created from given names reversed and 5 random characters added + '@gmail.com'
const createRandomEmails = ({names}) => {
  const emails = {
    emails: names.map(name => `${addRandomChars(name.split('').reverse().join(''), 5)}@gmail.com`)
  }
  createJson(emails)
}
